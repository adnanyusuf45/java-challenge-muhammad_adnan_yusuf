package ist.challenge.Muhammad.Adnan.Yusuf;

import ist.challenge.Muhammad.Adnan.Yusuf.Repository.UserRepository;
import ist.challenge.Muhammad.Adnan.Yusuf.entity.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserTest {

    @Autowired

    private UserRepository userRepository;

    // JUnit test for saveregister
    @Test
    @Order(1)
    @Rollback(false)
    public void saveUser(){

        User _user = User.builder()
                .username("test1")
                .password("test123")
                .build();
        userRepository.save(_user);

        assertThat(_user.getId()).isGreaterThan(0);
    }

    @Test
    @Order(2)
    @Rollback(false)
    public void loginUser(){

        User usernameEntry = userRepository.findByUsername("test1");


        assertThat(usernameEntry.getPassword().equals("test123"));
    }

    // JUnit test for listUser
    @Test
    @Order(3)
    @Rollback(false)
    public void listUser() {
        List<User> listUsernameEntry = userRepository.findAll();
        assertThatNoException();

    }

    @Test
    @Order(4)
    @Rollback(false)
    public void editUser() {
        Optional<User> userDatabyId = userRepository.findById(Long.valueOf(1));
        User _user = userDatabyId.get();
        _user.setUsername("test123");
        _user.setPassword("1234");
        userRepository.save(_user);

        Optional<User> userData2 = userRepository.findById(Long.valueOf(1));
        User _user2 = userData2.get();
        assertThat(_user2.getUsername()).isEqualTo("test123") ;
        assertThat(_user2.getPassword()).isEqualTo("1234") ;

    }

    @Test
    @Order(5)
    @Rollback(false)
    public void loginUserAfterEdit(){

        User usernameEntry = userRepository.findByUsername("test123");

        assertThat(usernameEntry.getPassword().equals("1234"));
    }
}

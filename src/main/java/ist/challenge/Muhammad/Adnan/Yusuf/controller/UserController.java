package ist.challenge.Muhammad.Adnan.Yusuf.controller;

import io.swagger.annotations.*;
import ist.challenge.Muhammad.Adnan.Yusuf.Repository.UserRepository;
import ist.challenge.Muhammad.Adnan.Yusuf.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @ApiOperation(value = "Registrasi", notes = "Registrasi")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Registrasi Sukses"),
            @ApiResponse(code = 409, message = "Username sudah terpakai"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    @PostMapping("/register")

    public ResponseEntity registerUser (@RequestBody User user){

        try {
            User usernameEntry = userRepository.findByUsername(user.getUsername().trim());
            if(usernameEntry!=null){

                return ResponseEntity.status(HttpStatus.CONFLICT).body("Username sudah terpakai");
            }
            User _user = User.builder()
                    .username(user.getUsername().trim())
                    .password(user.getPassword().trim())
                    .build();
            userRepository.save(_user);
            return ResponseEntity.status(HttpStatus.CREATED).body("Registrasi Sukses");

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
        }

    }

    @ApiOperation(value = "Login", notes = "Login")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Username dan / atau password kosong"),
            @ApiResponse(code = 401, message = "Username / Password Tidak Cocok"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    @PostMapping("/login")
    public ResponseEntity loginUser (@RequestBody User user){

        try {

            if(user.getUsername().trim()==null ||user.getUsername().trim().equals("") || user.getPassword().trim()==null||user.getPassword().trim().equals("")){

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username dan / atau password kosong");

            }


            User usernameEntry = userRepository.findByUsername(user.getUsername().trim());

            if(usernameEntry == null){
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Username / Password Tidak Cocok");
            }

            if(!usernameEntry.getPassword().equals(user.getPassword())){
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Username / Password Tidak Cocok");

            }

            return ResponseEntity.status(HttpStatus.OK).body("Sukses Login");

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
        }

    }

    @ApiOperation(value = "List User", notes = "List User")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    @GetMapping("/listuser")
    public ResponseEntity listUser (){

        try {

            List<User> listUsernameEntry = userRepository.findAll();

            return ResponseEntity.status(HttpStatus.OK).body(listUsernameEntry);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
        }

    }

    @ApiOperation(value = "Edit User", notes = "Edit User")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Edit User Berhasil"),
            @ApiResponse(code = 400, message = "Password tidak boleh sama dengan password sebelumnya"),
            @ApiResponse(code = 409, message = "Username sudah terpakai"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    @PutMapping("/edituser/{id}")
    public ResponseEntity editUser (@PathVariable("id") long id,@RequestBody User user){


        try {

            if(user.getUsername().trim()==null ||user.getUsername().trim().equals("") || user.getPassword().trim()==null||user.getPassword().trim().equals("")){

                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");

            }

            Optional<User> userDatabyId = userRepository.findById(id);

            if (userDatabyId.isPresent()) {

                if(userDatabyId.get().getUsername().trim().equals(user.getUsername().trim())){

                    if(userDatabyId.get().getPassword().trim().equals(user.getPassword().trim())){
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Password tidak boleh sama dengan password sebelumnya");
                    }

                    User _user = userDatabyId.get();
                    _user.setUsername(user.getUsername().trim());
                    _user.setPassword(user.getPassword().trim());
                    userRepository.save(_user);
                    return ResponseEntity.status(HttpStatus.CREATED).body("Edit User Berhasil");

                }else{

                    User usernameDatacheck = userRepository.findByUsername(user.getUsername().trim());

                    if(usernameDatacheck == null){
                        User _user = userDatabyId.get();
                        _user.setUsername(user.getUsername().trim());
                        _user.setPassword(user.getPassword().trim());
                        userRepository.save(_user);
                        return ResponseEntity.status(HttpStatus.CREATED).body("Edit User Berhasil");
                    }else{
                        return ResponseEntity.status(HttpStatus.CONFLICT).body("Username sudah terpakai");
                    }

                }

            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
            }

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
        }

    }


}

package ist.challenge.Muhammad.Adnan.Yusuf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MuhammadAdnanYusufApplication {

	public static void main(String[] args) {
		SpringApplication.run(MuhammadAdnanYusufApplication.class, args);
	}

}

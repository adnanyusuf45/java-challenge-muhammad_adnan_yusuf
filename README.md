# Show API JSON

Registrasi User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/Registrasi.PNG">

Login User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/login.PNG">

List User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/List_User.PNG">

Edit User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/Edit_User.PNG">

# Unit Test
<img src="https://gitlab.com/ist.challenge1/image-only/-/raw/main/Unit_Test.png">

# API Document
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/Swagger.PNG">

# Integrasi Unit Test

Registrasi User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/Swagger_Registrasi.PNG">

Login User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/Swagger_Login.PNG">

List User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/swagger_listuser.PNG">

Edit User
<img src="https://gitlab.com/adnanyusuf45/java-challenge-image-doc/-/raw/main/swagger_edituser.PNG">
